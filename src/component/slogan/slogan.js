import React, { Component } from 'react'
import './slogan.scss';

export default class slogan extends Component {
    render() {
        return (
            <div className="sloganContainer">
                <h2 className="sloganContainer_header">{this.props.header}</h2>
                <p className="sloganContainer_title">{this.props.title}</p>
            </div>
        )
    }
}
