import React, { Component } from 'react'
import './score.scss'

export default class Score extends Component {
    render() {
        return (
            <div className="scoreContainer">
                <ul className="centerContainer skillList">
                    <li className="skillList_skillSection">
                        <span className="icon_portfolio"></span>
                        <div>
                            <p className="skillList_score">548</p>
                            <p className="skillList_title">projects completed</p>
                        </div>
                    </li>
                    <li className="skillList_skillSection">
                        <span className="icon_watch"></span>
                        <div>
                            <p className="skillList_score">1465</p>
                            <p className="skillList_title">working hours</p>
                        </div>
                    </li>
                    <li className="skillList_skillSection">
                        <span className="icon_star"></span>
                        <div>
                            <p className="skillList_score">612</p>
                            <p className="skillList_title">positive feedbacks</p>
                        </div>
                    </li>
                    <li className="skillList_skillSection">
                        <span className="icon_like"></span>
                        <div>
                            <p className="skillList_score">735</p>
                            <p className="skillList_title">happy clients</p>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}
