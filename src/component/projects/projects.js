import React, { Component } from 'react';
import './project.scss';

export default class Project extends Component {
    render() {
        return (
            <ul className="projectContainer">
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
                <li className="projectContainer_project">
                    <img src={ require('./img/eye-regular.png') } alt="eye" className="projectContainer_eyeIcon"/>
                </li>
            </ul>
        )
    }
}
