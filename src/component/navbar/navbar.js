import React, { Component } from 'react';
import './navbar.scss';

export default class Navbar extends Component {
    render() {
        return (
            
                <nav className='navbar'>
                    <div className="centerContainer">
                        <img src={ require('./img/logo.png') } alt="logo"/>
                        <ul className='navbarMenu'>
                            <li className='navbarMenu_btn'>Home</li>
                            <li className='navbarMenu_btn'>About</li>
                            <li className='navbarMenu_btn'>Projects</li>
                            <li className='navbarMenu_btn'>Contact</li>
                        </ul>
                    </div>
                </nav>
            
        )
    }
}
