import React, { Component } from 'react';
import './contact.scss';

export default class Contact extends Component {
    render() {
        return (
            <form action="#" className="contactInputContainer">
                <div className="halfInput_container">
                    <input type="text" className="contactInputContainer_halfInput" placeholder="Your Name"/>
                    <input type="text" className="contactInputContainer_halfInput" placeholder="Your Email"/>
                </div>
                <input type="text" className="contactInputContainer_fullInput" placeholder="Title"/>
                <textarea cols="30" rows="10" className="contactInputContainer_fullInput" placeholder="Your Comment"></textarea>
                <input type="submit" value="send message" className="contactInputContainer_submitBtn"/>
            </form>
        )
    }
}
