import React, { Component } from 'react'
import './footer.scss'

export default class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="centerContainer">
                    <div className="footer_rights">
                        <p className="footer_companyName">Piroll Design, Inc.</p>
                        <p className="footer_title">© 2017 Piroll. All rights reserved.
                            <br/>
                            Designed by robirurk.
                        </p>
                    </div>
                    <p className="footer_contact">hello@pirolltheme.com
                        <br/>
                        +44 987 065 908
                    </p>
                    <ul className="footer_navbar">
                        <li className="footer_title">Home</li>
                        <li className="footer_title">About</li>
                        <li className="footer_title">Projects</li>
                        <li className="footer_title">Contact</li>  
                    </ul>
                </div>
            </div>
        )
    }
}
