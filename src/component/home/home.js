import React, { Component } from 'react'
import Slogan from '../slogan/slogan'
import Project from '../projects/projects'
import Score from '../score/score'
import ReactFancyBox from 'react-fancybox'
import Contact from '../contact/contact'
import Footer from '../footer/footer'

// CSS
import '../../general.scss'; import './scss/welcome.scss'; import './scss/aboutUs.scss'; import './scss/skill.scss'; import './scss/loadBtn.scss'; 
import './scss/workProcess.scss'; import 'react-fancybox/lib/fancybox.css'; import './scss/ourService.scss'; import './scss/feedback.scss'; 
import './scss/customer.scss'; import './scss/contactUs.scss';



export default class Home extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="welcomePage">
                    <div className='centerContainer'>
                        <div className='welcomeTextContainer'>
                            <h1 className='welcomeTextContainer_header'>We Design and Develop</h1>
                            <p className='welcomeTextContainer_title'>We are a new design studio based in USA. We have over  20 years of combined experience, and know a thing or two about designing websites and mobile apps.</p>
                            <a href="https://www.youtube.com" className='welcomeTextContainer_contactBtn'>contact us</a>
                        </div>
                    </div>
                </div>
                <div className="aboutUsContainer centerContainer">
                    <Slogan 
                        header = "About Us"
                        title = "Divide have don't man wherein air fourth. Own itself make have night won't make. A you under Seed appear which good give. Own give air without fowl moveth dry first heaven fruit, dominion she'd won't very all."
                    />
                    <img src={ require('./img/signature.png') } alt="signature" className='aboutUsContainer_signature'/>
                </div>
                <div className="skillContainer">
                    <div className="skillContainer_info">
                        <ul className="skillsList">
                            <li>
                            <h2 className="skillContainer_infoHeader">Professional Skills</h2>
                            </li>
                            <li className="skillsList_skill">
                                <p>ui/ux design 75%</p>
                                <div className="progressBarContainer">
                                    <div className="progressBarContainer_progressBar"></div>
                                </div>
                            </li>
                            <li className="skillsList_skill">
                                <p>web development 90%</p>
                                <div className="progressBarContainer">
                                    <div className="progressBarContainer_progressBar"></div>
                                </div>
                            </li>
                            <li className="skillsList_skill">
                                <p>marketing 65%</p>
                                <div className="progressBarContainer">
                                    <div className="progressBarContainer_progressBar"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="skillContainer_photo"></div>
                </div>
                <Score />
                <Project />
                <button className="loadMoreBtn">load more work</button>
                <div className="workProcessContainer">
                    <div className="centerContainer">
                        <Slogan 
                            header = "Our Work Process"
                            title = "Was years it seasons was there form he in in them together over that, third sixth gathered female creeping bearing behold years."
                        />
                        <ReactFancyBox
                            thumbnail={ require('./img/video_background2.jpg') }
                            image={ require('./img/video_background2.jpg') }
                        />
                    </div>
                </div>
                <div className="ourServiceContainer">
                    <ul className="centerContainer">
                        <li className="ourService">
                            <span class="serviceIcon_ui"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">ui/ux design</p>
                            <p className="ourService_description">
                                Be set fourth land god darkness make it wherein own
                            </p>
                        </li>
                        <li className="ourService">
                        <span class="serviceIcon_web"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">web developmnent</p>
                            <p className="ourService_description">
                                A she'd them bring void moving third she'd kind fill
                            </p>
                        </li>
                        <li className="ourService">
                        <span class="serviceIcon_mobile"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">app/mobile</p>
                            <p className="ourService_description">
                                Dominion man second spirit he, earth they're creeping
                            </p>
                        </li>
                        <li className="ourService">
                        <span class="serviceIcon_game"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">game design</p>
                            <p className="ourService_description">
                                Morning his saying moveth it multiply appear life be
                            </p>
                        </li>
                        <li className="ourService">
                        <span class="serviceIcon_seo"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">seo/marketing</p>
                            <p className="ourService_description">
                                Give won't after land fill creeping meat you, may
                            </p>
                        </li>
                        <li className="ourService">
                        <span class="serviceIcon_photography"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">photography</p>
                            <p className="ourService_description">
                                Creepeth one seas cattle grass give moving saw give
                            </p>
                        </li>
                        <li className="ourService">
                        <span class="serviceIcon_graphic"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">graphic design</p>
                            <p className="ourService_description">
                                Open, great whales air rule for, fourth life whales
                            </p>
                        </li>
                        <li className="ourService">
                            <span class="serviceIcon_illustration"></span>
                            <span className="ourService_icon"></span>
                            <p className="ourService_type">illustartions</p>
                            <p className="ourService_description">
                                Whales likeness hath, man kind for them air two won't
                            </p>
                        </li>
                    </ul>
                </div>
                <div className="feedBackContainer">
                    <div>
                        <p className="feedBackContainer_feedback">
                        “ Outstanding job and exceeded all expectations. It was a pleasure to work with them on a sizable first project and am looking forward to start the next one asap.”
                        </p>
                        <p className="feedBackContainer_customer">Michael Hopkins</p>
                    </div>
                </div>
                <div className="customerContainer centerContainer">
                    <img src={ require('./img/client1.png') } alt="Company"/>
                    <img src={ require('./img/client2.png') } alt="Company"/>
                    <img src={ require('./img/client3.png') } alt="Company"/>
                    <img src={ require('./img/client4.png') } alt="Company"/>
                    <img src={ require('./img/client5.png') } alt="Company"/>
                </div>
                <div className="contactUsContainer">
                    <Slogan 
                        header = "Need a Project?"
                        title = "Let us know what you're looking for in an agency. We'll take a look and see if this could be the start of something beautiful."
                    />
                    <Contact />
                </div>
                <Footer />
            </React.Fragment>
        )
    }
}
