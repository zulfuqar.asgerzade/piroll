import React from 'react';
import Home from './component/home/home'
import Navbar from './component/navbar/navbar'
import './App.css';

function App() {
  return (
    <React.Fragment>
      <Navbar />
      <Home />
    </React.Fragment>
  );
}

export default App;
